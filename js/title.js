async function title() {
    const titles = ["Srdcem Robinson", "Myslí Robinson", "Tělem Robinson", "Duší Robinson"];
    while(true) {
        for (let i = 0; i < titles.length; i++) {
            document.title = titles[i];
            await new Promise(r => setTimeout(r, 2500));
        }
    }
}